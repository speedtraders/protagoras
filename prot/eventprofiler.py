'''
Created on December, 2012

@author: Georgios Pierris
@contact: gpierris@gmail.com
@summary: Event Profiler that later writes the events to a file.
'''


from QSTK.qstkutil import DataAccess as da
import numpy as np
import copy
import csv
import QSTK.qstkutil.qsdateutil as du
import datetime as dt


class Event:

    def __init__(self, svm=None):
        self.defineEvent()

    #Change the initializations to this function to define a different Event
    def defineEvent(self):

        self.storename = 'Yahoo'
        self.dataobj = da.DataAccess(self.storename)
        self.closefield = 'actual_close'
        self.volumefield = 'volume'
        self.window = 10

        self.startDay = dt.datetime(2009, 1, 1)
        self.endDay = dt.datetime(2010, 12, 31)
        self.timeofday = dt.timedelta(hours=16)
        self.timestamps = du.getNYSEdays(self.startDay, self.endDay, self.timeofday)
        #self.symbols = ['AAPL', 'GOOG', 'SPY']
        self.symbols = self.dataobj.get_symbols_from_list("sp5002012")
        self.symbols.append('SPY')
        self.symbolsForReference = ['SPY']

    def isEvent(self, closePriceToday, closePriceYesterday):
        eventPrice = 7.00

        if(closePriceToday < eventPrice and closePriceYesterday >= eventPrice):
            return 1
        else:
            return 0


class EventProfiler:

    def __init__(self, outFilename = "orders.csv", ):

        self.outFilename = outFilename
        self.event = Event()

    def getMarketData(self, marketData):
        self.marketData = marketData


    def runEventProfiler(self):
        print 'Running Event Profiler...'
        return self.findEvents()


    def findEvents(self, verbose=False):

        # Reading the Data for the list of Symbols.
        timestamps = du.getNYSEdays(self.event.startDay,self.event.endDay,self.event.timeofday)
        dataobj = da.DataAccess(self.event.storename)
        if verbose:
                print __name__ + " reading data"
        # Reading the Data
        close = dataobj.get_data(timestamps, self.event.symbols, self.event.closefield)

        # Completing the Data - Removing the NaN values from the Matrix
        close = (close.fillna(method='ffill')).fillna(method='backfill')


        # Calculating Daily Returns for the Market
        #tsu.returnize0(close.values) #my change. We did not want to take the daily returns
        #referenceValues = close[self.event.symbolsForReference]

        # Calculating the Returns of the Stock Relative to the Market
        # So if a Stock went up 5% and the Market rised 3%. The the return relative to market is 2%
        mktneutDM = close
        np_eventmat = copy.deepcopy(mktneutDM)
        for sym in self.event.symbols:
            for time in timestamps:
                np_eventmat[sym][time]=np.NAN

        if verbose:
                print __name__ + " finding events"


        for symbol in self.event.symbols:
            for i in range(1,len(mktneutDM[symbol])):

                np_eventmat[symbol][i] = self.event.isEvent(mktneutDM[symbol][i], mktneutDM[symbol][i-1])


        return np_eventmat

    def saveEventToOrders(self, eventmat, inFilename):

        with open(inFilename, "wb") as f:
            fileWriter = csv.writer(f, delimiter=',')

            for symbol in self.event.symbols:

                for i in range(1, len(eventmat[symbol])):

                    if( eventmat[symbol][i] == 1):

                        fileWriter.writerow([str(self.event.timestamps[i].year), str(self.event.timestamps[i].month), str(self.event.timestamps[i].day), symbol, 'Buy', '100', ''])
                        try:
                            laterTimestamp = self.event.timestamps[i+5]
                        except:
                            laterTimestamp = self.event.timestamps[-1]

                        fileWriter.writerow([str(laterTimestamp.year), str(laterTimestamp.month), str(laterTimestamp.day), symbol, 'Sell', '100', ''])

                    elif( eventmat[symbol][i] == -1 ):

                        fileWriter.writerow([str(self.event.timestamps[i].year), str(self.event.timestamps[i].month), str(self.event.timestamps[i].day), symbol, 'Sell', '100', ''])
                        try:
                            laterTimestamp = self.event.timestamps[i+5]
                        except:
                            laterTimestamp = self.event.timestamps[-1]

                        fileWriter.writerow([str(laterTimestamp.year), str(laterTimestamp.month), str(laterTimestamp.day), symbol, 'Buy', '100', ''])



    def addDays(self, timestamp, days):

        return timestamp + dt.timedelta(days=days)

