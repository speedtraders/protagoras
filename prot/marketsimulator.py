'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import csv
import copy
import datetime as dt

import portfolio as ptf
from prot import marketdata_accessor as MDA


class MarketSimulator():

    def __init__(self, startingCash=1000000, ordersFilename="", valuesFilename=""):

        self.ordersFilename = ordersFilename
        self.orders = []
        self.valuesFilename = valuesFilename


        self.marketData = MDA.MarketDataAccessor()
        self.portfolio = ptf.Portfolio(startingCash, self.marketData)

        self.initializationSanity()


    def initializationSanity(self):

        if(self.portfolio.startingCash <= 0):
            self.printUsage()
            self.portfolio.startingCash = 1000000
            self.portfolio.setCash(availableCash = 1000000)
            print "\n\nWARNING: I will proceed with cash=$1,000,000!"

        if(self.ordersFilename is ""):
            print 'Not valid orders file! Market simulator will ignore it and run without executing orders!'


        if(self.valuesFilename is ""):
            print 'Haven\'t received an output file name. Market Simulator will continue and export the results at values.csv'
            self.valuesFilename = "values.csv"



    def loadOrders(self):

        with open(self.ordersFilename, 'rU') as csvfile:
            dataReader = csv.reader(csvfile, delimiter=',')
            for row in dataReader:
                self.orders.append(row)

        #Sanity check to remove empty string from various newlines in OSes
        for idx,order in enumerate(self.orders):
            self.orders[idx] = filter( lambda a: a!='', order )
            self.orders[idx].append('p') # p for Pending, c for completed
            for iidx, _ in enumerate(self.orders[idx]):
                if(iidx<=2 or iidx == 5):
                    self.orders[idx][iidx] = int(self.orders[idx][iidx])


        self.orders.sort()


    def executeOrder(self,order, timestamp):

        if order[-1] == 'c': #Then it is already completed
            print 'Warning! Order\t', order, '\thas already been completed'
            return 1

        stockPrice = self.marketData.stockdata[self.getStockName(order)][timestamp]

        #Find type of order BUY or SELL
        if self.getOrderType(order) == 'Buy':

            success = self.portfolio.buyEquity(self.getStockName(order), stockPrice, \
                    self.getOrderVolume(order), ignoreHavingEnoughCash=True)


        elif self.getOrderType(order) == 'Sell':

            success = self.portfolio.sellEquity(self.getStockName(order),
                    stockPrice, self.getOrderVolume(order),\
                    allowShortSelling=True)

        if(success):
            print 'Order successfully completed! ', self.getOrderType(order),' ', self.getStockName(order),' ', self.getOrderVolume(order), ' at $ ', stockPrice
            return 1
        else:
            print 'Could not complete order.'
            return 0


    def runSim(self):

        self.startDay = self.getStartingDateOfOrders()
        self.endDay = self.getEndingDateOfOrders()

        timestamps = self.marketData.getTradingDays(self.startDay, self.endDay)
        symbols = self.getSymbolListFromOrders()
        symbols.append('SPY')
        symbols.append('$SPX') #Add additional benchmarks, or options you will need in the portfolio analyzer later.


        #Fetch market data all in once!
        print 'Please be patient... Fetching data...'
        print 'From', self.startDay, ' until ', self.endDay, '\nfor symbols:\n', symbols
        self.marketData.load_data_from_qstk(self.startDay, self.endDay, symbols, data_items = ['actual_close'])
        print '\nFetching data completed. Execution continues...\n\n'

        #Experimental to save data to pickle to avoid using installation of QSTK to run a simple backtesting
        #self.marketData.saveDataToDisk()

        for timestamp in timestamps: # That is the main Simulation every day
            print '\t\t\t\t', timestamp.strftime('%Y-%m-%d')

            for idx, curOrder in enumerate(self.orders):

                #Ignore completed, try again for failed or pending
                if(curOrder[-1] == 'c'):
                    continue

                curOrderDate = self.getOrderDate( curOrder )

                if( timestamp>=curOrderDate):
                    if( self.executeOrder(curOrder, timestamp) ):
                        self.orders[idx][-1] = 'c'#Completed
                    else:
                        self.orders[idx][-1] = 'f' #Failed

            self.portfolio.updateDailyPortfolio(timestamp)


        self.portfolio.saveDailyFundsToFile(self.valuesFilename)

        self.portfolioAnalyzer = ptf.PortfolioAnalyzer(copy.deepcopy(self.portfolio), benchmark='$SPX')

        self.portfolioAnalyzer.plotBenchmarkAndPortfolio()

    def bindEventProfiler(self, eventProfiler):

        #In the future we should have multiple event profilers and run in parallel simulations...
        self.myEventProfiler = eventProfiler
        self.myEventProfiler.getMarketData(self.marketData)


    def getSymbolListFromOrders(self):

        symbols = []
        for order in self.orders:
            symbols.append( self.getStockName(order) )

        #Unify the list of symbols.
        my_set = {}
        map(my_set.__setitem__, symbols, [])
        symbols = my_set.keys()

        return symbols



    def printOrders(self):
        print '\n\nAll Orders'
        print '-------------'
        print 'Year,\tMonth,\tDay,\tName,\tType,\tVolume,\tStatus'
        for order in self.orders:
            for element in order:
                print element,'\t',
            print ''
        print '\n\n'

    def getStockName(self, order):
        return order[3]

    def getOrderVolume(self, order):
        return int( order[5] )

    def getOrderDate(self,order):

        tempDate = map( int, order[0:3] )
        orderDate = dt.datetime(tempDate[0], tempDate[1], tempDate[2], hour=16)

        return orderDate

    def isEqualDate(self,date1, date2):

        if (date1.year==date2.year and date1.month==date2.month and date1.day ==date2.day):

            return 1
        else:
            return 0

    def saveResults(self):
        pass

    def getStartingDateOfOrders(self):
        tempDate = map( int, self.orders[0][0:3] )
        tempDate = dt.datetime(tempDate[0], tempDate[1], tempDate[2], hour=16)

        return tempDate

    def getEndingDateOfOrders(self):
        tempDate = map( int, self.orders[-1][0:3] )
        tempDate = dt.datetime(tempDate[0], tempDate[1], tempDate[2], hour=16)

        return tempDate

    def getOrderType(self, order):
        return order[4] #We do it as a function to easily change it in the future

    def printUsage(self):

        print "\nWelcome to the market simulator. \n\nUsage: \n"
        print "-c cash \t: The starting amount of cash, e.g., -c 1000000, or --cash=1000000"
        print "-o orders.csv\t: The orders' file input. Also --orders=orders.csv"
        print "-v values.csv\t: The output file name. Does not need to exist.It will override previous simulations if filename is the same! Also --values=values.csv\n\n"


    def printStatistics(self):

        print '\n\nStatistics'
        print '----------------'

        print 'Total Return:\t\t\t', self.portfolioAnalyzer.getTotalReturn()
        print 'Total Cumulative Return:\t', self.portfolioAnalyzer.getTotalCumulativeReturn()
        print 'Sharpe Ratio:\t\t\t', self.portfolioAnalyzer.getSharpeRatio()
        print '\n\n'

    def getStatistics(self):
        return self.portfolioAnalyzer.getTotalCumulativeReturn(), self.portfolioAnalyzer.getSharpeRatio()

