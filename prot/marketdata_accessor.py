'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

#This module acts as an intermediate market data accessor so we can be
#independent of QSTK and use mt5 historical data as well,
#or other in the future.

try:
    from QSTK.qstkutil import DataAccess as da
    import QSTK.qstkutil.qsdateutil as du
    QSTK = True
except:
    QSTK = False

import datetime as dt
import copy
import pickle
import csv

class MarketDataAccessor:

    def __init__(self):

        if(QSTK):
            self.data_obj = da.DataAccess('Yahoo')
            self.symbols = []
            self.mt5 = False

        else:
            print 'QSTK is not available!'
            self.stockdata = {}
            self.samples = []
            self.mt5 = True


    '''
        Metatrader 5 functions
    '''

    def load_data_from_mt5(self, historicaldata_filename, symbol='EURUSD'):


        self.mt5 = True
        self.samples = []


        # Clean fron NULL bytes and keep a local copy
        while(1):
            try:
                f_i = open(historicaldata_filename, 'rb')
                data = f_i.read()
                f_i.close()
                break
            except:
                print 'File not ready yet'
                continue

            f_o = open('data\\temp.txt', 'wb')
            f_o.write(data.replace('\x00', ''))
            f_o.close()

            with open('data\\temp.txt', 'rb') as csvfile:
                data_reader = csv.reader(csvfile, delimiter=',')
                for row in data_reader:
                    self.samples.append(row)

            self.timestamps = []
            self.stockdata[symbol] = {}

            for idx, _ in enumerate(self.samples):
                for iidx, _ in enumerate(self.samples[idx]):
                    if(iidx == 0):
                        # print self.samples[idx][iidx]
                        date, time = self.samples[idx][iidx].split(' ')
                        year, month, day = date.split('.')
                        hour, minute, second = time.split(':')
                        self.samples[idx][iidx] = dt.datetime(int(year),
                                                              int(month),
                                                              int(day),
                                                              int(hour),
                                                              int(minute),
                                                              int(second))
                        self.timestamps.append(self.samples[idx][iidx])
                    else:
                        self.samples[idx][iidx] = float(self.samples[idx][iidx])

                self.stockdata[symbol][self.timestamps[-1]] = \
                copy.deepcopy(self.samples[idx][1:])

            # print self.stockdata[symbol][self.timestamps[-1]][-1]
            # print type(self.samples[0][0])
            # print self.samples[0][1]

    def saveDataToDisk(self):

        fileOut = open("marketData.pck", "w") #write mode
        pickle.dump(self.stockdata, fileOut)

    def loadDataFromDisk(self):
        fileIn = open("marketData.pck", "r") #read mode
        self.stockdata = pickle.load(fileIn)


    def set_symbols(self, symbols, filename=''):
        #This is to set custom file for symbols to load
        if(not symbols): #Check for empty list
            self.symbols = self.data_obj.get_symbols_from_list(filename)
        else:
            self.symbols = symbols

        return self.symbols

    def load_data_from_qstk(self,
                            start_day, end_day, symbols,
                            data_items=['actual_close']):

        #This function call should happen once to avoid downloading data etc.
        timeOfDay = dt.timedelta(hours = 16)
        self.timestamps = du.getNYSEdays(start_day, end_day, timeOfDay)

        if( not symbols): #Pythonic! Checks for empty list
            symbols = self.symbols #Take it from file

        #Because the data_items is a list it returns a list of stockdata.
        #For now we use only one so we take the first DataFrame!
        data = self.data_obj.get_data(self.timestamps, symbols, data_items)

        self.stockdata = data[0]

        self.stockdata = (self.stockdata.fillna(method='ffill'))\
                                                .fillna(method='backfill')

        #Prepare the daily returns as well
        self.daily_returns = copy.deepcopy( self.stockdata )
        for symbol in symbols:
            for tIdx in range(1, len(self.timestamps)):
                self.daily_returns[symbol][self.timestamps[tIdx]] = \
                self.stockdata[symbol][self.timestamps[tIdx]]/\
                self.stockdata[symbol][self.timestamps[tIdx-1]]

        #print self.daily_returns


    def getTradingDays(self, startDay, endDay):
        timeOfDay = dt.timedelta(hours=16)
        timestamps = du.getNYSEdays(startDay, endDay, timeOfDay)

        return timestamps


if(__name__ == "__main__"):
    MDA = MarketDataAccessor()
    DATA_FILE = "C:\\Users\\gpierris\\AppData\\Roaming\\MetaQuotes\\Terminal\\\
    D0E8209F77C8CF37AD8BF550E51FF075\\MQL5\\Files\\data\\\
    HistoricalData_2013.01.01_To_2013.12.31.txt"
    print DATA_FILE
    MDA.load_data_from_mt5(DATA_FILE, 'EURUSD')


