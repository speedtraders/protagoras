'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import csv
import math

import numpy as np
import matplotlib.pyplot as plt
#from pylab import *


class Equity:

    def __init__(self, name='', price=0.00, volume=0):
        self.name = name
        self.price = price
        self.volume = volume


class PortfolioAnalyzer:

    def __init__(self, portfolio, benchmark='$SPX'):

        self.benchmark = benchmark
        self.portfolio = portfolio


        if( type(portfolio) == str):

            self.benchmark = benchmark
            self.loadPortfolioFromFile(portfolio) #Then this is a filename

        else:


            self.startingCash = portfolio.startingCash
            self.timestamps = portfolio.dailyTimestamps
            self.fundsValue = portfolio.dailyFundsValue

    def loadPortfolioFromFile(self, valuesFilename):
        pass

    def getNormalized(self, dataIn):

        return [ x/dataIn[0] for x in dataIn ]

    def getBenchmarkPerformance(self):

        data =  self.portfolio.marketData.stockdata[self.benchmark][self.timestamps]

        return data

    def getTotalReturn(self):

        return self.fundsValue[-1]


    def getTotalCumulativeReturn(self):
        return (self.fundsValue[-1]/self.fundsValue[0]) - 1.0

    def getSharpeRatio(self):

        dailyReturns = self.getDailyReturns()

        mean = np.mean(dailyReturns)
        std = np.std(dailyReturns)

        return math.sqrt(len(dailyReturns))* mean/std

    def getDailyReturns(self):

        dailyReturns = []
        dailyReturns.append(0.0)

        for i in range(1,len(self.fundsValue)):
            dailyReturns.append( self.fundsValue[i] / self.fundsValue[i-1] - 1.0 )

        return dailyReturns

    def plotBenchmarkAndPortfolio(self):

        normalizedPortfolio = self.getNormalized(self.fundsValue)
        benchmarkPerformance = self.getNormalized(self.getBenchmarkPerformance())


        plt.clf()
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(self.timestamps, normalizedPortfolio, label='Portfolio')
        ax.plot(self.timestamps, benchmarkPerformance, label=self.benchmark)
        ax.legend(loc='lower left')
        plt.ylabel('Cumulative Returns')
        plt.xlabel('Date')
        fig.autofmt_xdate(rotation=45)

        ax.grid('on')
        fig.savefig('portfolio.pdf', format='pdf')



        #savefig('portfolio.pdf',format='pdf')


class Portfolio:

    def __init__(self, startingCash, marketData):

        self.marketData = marketData

        self.startingCash = startingCash
        self.cash = startingCash
        self.borrowedFunds = 0.0

        self.equities = {} # A quick dictionary to find aggregated volume
        self.equitiesHistory = [] #Detailed history of orders with price and volume
        self.dailyFundsValue = []
        self.dailyTimestamps = []

    def setCash(self, availableCash=1000000):
        self.cash = availableCash

    def depositCash(self, amount):
        self.cash = self.cash + amount

    def withdrawCash(self, amount):
        if( self.cash >= amount ):
            self.cash = self.cash - amount
            return 1
        else:
            print 'Not enough funds. Available are ', self.cash
            return 0

    def buyEquity(self, stockName, stockPrice, volume, ignoreHavingEnoughCash=False):

        if( ignoreHavingEnoughCash or self.cash >= stockPrice * volume):
            try:#Exception if it the first time we buy
                self.equities[stockName] = self.equities[stockName] + volume
            except:
                self.equities[stockName] = volume

            self.equitiesHistory.append( Equity(stockName, stockPrice, volume ) )
            self.cash = self.cash - stockPrice*float(volume)

            return 1

        else:
            print 'Not enough cash!'
            return 0


    def sellEquity(self, stockName, stockPrice, volume, allowShortSelling=False):
        #allowShortSelling is a flag to allow selling even if we haven't bought the equites, assuming we had them from the past...
        #First we have to verify that we have the stocks!!!
        totalVolOfStock = 0
        goingShort = False
        try: # We will have an exception if we don't have any
            totalVolOfStock = self.equities[stockName]
            if(totalVolOfStock<0):
                raise Exception('Negative number of Stocks!')
        except:
            if(allowShortSelling):#That means we are short selling
                goingShort = True
                self.equities[stockName] = 0
            else:
                return 0

        if( totalVolOfStock >= volume): #Then we have enough to sell

            self.cash = self.cash + stockPrice * volume
            self.equities[stockName] = totalVolOfStock - volume

        elif(goingShort or totalVolOfStock<volume):

            '''
            if(not goingShort):
               #FIXME: IT IS NOT WORKING PROPERLY

                print 'Warning! Short selling not allowed! Not enough stocks to sell.'
                print 'Will proceed by selling only your', totalVolOfStock, ' stocks'

                self.cash = self.cash + stockPrice * totalVolOfStock
                self.equities[stockName] = self.equities[stockName] - totalVolOfStock
            '''
            self.cash = self.cash + stockPrice * volume
            self.equities[stockName] = self.equities[stockName] - volume

        return 1

    def updateDailyPortfolio(self, timestamp):

        #print self.equities
        stockNames = self.equities.keys()

        if( len(stockNames) < 0 ):
            self.dailyFundsValue.append(self.cash)
            self.dailyTimestamps.append(timestamp)
            return


        totalFundsValue = 0.0

        for stock in stockNames:
            stockPrice = self.marketData.stockdata[stock][timestamp]
            totalFundsValue = totalFundsValue + self.equities[stock] * stockPrice


        self.dailyFundsValue.append(self.cash+totalFundsValue)
        self.dailyTimestamps.append(timestamp)


    def saveDailyFundsToFile(self, outFilename):

        with open(outFilename, "wb") as f:
            fileWriter = csv.writer(f, delimiter=',', quotechar=' ',\
                    quoting=csv.QUOTE_MINIMAL)

            for idx, dailyFund in enumerate(self.dailyFundsValue):
                fileWriter.writerow([','.join([str(self.dailyTimestamps[idx].year), str(self.dailyTimestamps[idx].month), str(self.dailyTimestamps[idx].day)]), str(dailyFund)])

