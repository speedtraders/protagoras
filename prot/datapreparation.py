'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import numpy as np
import copy
import sys


class DataPreparation:

    def __init__(self, stock_market=None):
        self.stock_market_data = stock_market

    #Each addFeature_XXX should add a column to the self.x

    def getfeature_stockorindex(self, symbol, daysBack=2):

        x=[]
        for tsIdx, _ in enumerate(self.stock_market_data.timestamps):

            if(tsIdx-daysBack < 0):
                continue
            timestamps = self.stock_market_data.timestamps[tsIdx-daysBack:tsIdx]
            #print timestamps
            x.append(self.stock_market_data.stockdata[symbol][timestamps].values)

        return np.array(x)


    def appendColumns(self, parent_matrix, additional_columns):

        parent_matrix = np.array(parent_matrix)
        additional_columns = np.array(additional_columns)

        #print parent_matrix.shape[0], parent_matrix.shape[1]
        #print additional_columns.shape[0]

        return np.append(parent_matrix, additional_columns, axis=1)


    #Not a proper Derivative though...
    def derivativeOf(self, inVec):

        inDerivative = copy.deepcopy(inVec)

        #print type(inDerivative[0])

        if( type(inDerivative[0])==list or type(inDerivative[0])==np.ndarray):
            dim = 2 # Then we have 2D matrix
        else:
            dim = 1 # just a vector

        if(dim==1):
            for idx in range(len(inDerivative)):
                if(idx==0):
                    inDerivative[idx] = 1.0
                else:
                    inDerivative[idx] = inVec[idx]/inVec[idx-1]

        else:
            for r_idx in range(len(inDerivative)):
                for c_idx in range(len(inDerivative[0])):
                    if(c_idx == 0):
                        inDerivative[r_idx][c_idx] = 1.0
                    else:
                        if(inVec[r_idx][c_idx-1] != 0):
                            inDerivative[r_idx][c_idx] = \
                            inVec[r_idx][c_idx]/inVec[r_idx][c_idx-1]
                        else:
                            inDerivative[r_idx][c_idx] = 1.0


        return np.array(inDerivative)

    def defineClasses(self, inVec, thresholds = [0.98, 1.02]):

        for idx in range(inVec.shape[0]):
            if(inVec[idx]<thresholds[0]):
                inVec[idx] = -1
            elif(inVec[idx]>thresholds[1]):
                inVec[idx] = 1
            else:
                inVec[idx] = -1

        return inVec

    def increaseDimensionality(self, inVec):
        #Create new feature my multiplying them with the others
        outVec = copy.deepcopy(inVec)

        for featureI in range(np.size(inVec[0,:])): #Each feature multiply it with all the rest
            for featureJ in range(np.size(inVec[0,:])):
                newFeature = np.multiply(inVec[:, featureI], inVec[:, featureJ])
                newFeature = np.reshape(newFeature, (np.size(newFeature[:]), 1))
                #print np.size(newFeature[:])
                #print np.size(outVec[:,0]), np.size(outVec[0,:])
                #print newFeature
                outVec = self.appendColumns(outVec, newFeature)

        return outVec

    #Depending on the desired features you should write your own prepareData_xxx
    #functions to call.

    # These functions should return two vectors/matrices. The feature vector
    # should be an m by n matrix, where m rows represent the different training
    # samples, and n columns represent the features. The other m by 1 vector
    # is either the classification class, e.g. 1/-1, or a real value for
    # regression



    def prepareData_AllFeatures(self, daysBack=2, MT5valueColumn = 0 , barsAhead = 1, predictionMode = False):

        #We will return these two vectors
        x, y = [],  []


        #print self.stock_market_data.timestamps
        for tsIdx, _ in enumerate(self.stock_market_data.timestamps):
            #print tsIdx, daysBack, tsIdx+barsAhead, len(self.stock_market_data.timestamps), predictionMode

            if( tsIdx >= daysBack -1 ):
                pass
            else:
                continue

            #print 'I am in'
            timestamps = self.stock_market_data.timestamps[tsIdx-daysBack+1:tsIdx+1]
            #print timestamps
            for symbol in self.stock_market_data.symbols:

                singleSample = []
                regressionResult = None
                #print type(self.stock_market_data.stockdata['EURUSD'][timestamps[0]])
                #print self.stock_market_data.stockdata['EURUSD'][timestamps[0]]
                try:

                    if(self.stock_market_data.mt5):

                        singleSample = []
                        for i in range(len(self.stock_market_data.stockdata[symbol][timestamps[0]])):
                            for s in timestamps:
                                singleSample.append( self.stock_market_data.stockdata[symbol][s][i] )
                        try:
                            regressionResult = \
                            self.stock_market_data.stockdata[symbol][self.stock_market_data.timestamps[tsIdx+barsAhead]][MT5valueColumn]
                        except IndexError:
                            if(predictionMode):
                                #We are in prediction mode and  there is no regression result
                                regressionResult = None
                            else:
                                continue

                    else:

                        print 'ERROR: prepareData_AllFeatures has not been implemented yet for QSTK!\n\nProgram will now exit...'
                        sys.exit()


                    #print singleSample, '\t', regressionResult
                    x.append(singleSample)
                    y.append(regressionResult)

                except IndexError:
                    #The exception happens at the first daysBack, as well as the
                    #last few timestamps
                    pass#print 'Error\t', tsIdx
                    print 'Error!!!!'

        return np.array(x), np.array(y)





    def prepareData_lastPrices(self, daysBack=2, MT5valueColumn = 1 ):

        #We will return these two vectors
        x, y = [],  []

        for tsIdx, _ in enumerate(self.stock_market_data.timestamps):

            if(tsIdx-daysBack<0):
                continue
            timestamps = self.stock_market_data.timestamps[tsIdx-daysBack:tsIdx+1]
            #print timestamps
            for symbol in self.stock_market_data.symbols:

                singleSample = []
                regressionResult = None
                #print type(self.stock_market_data.stockdata['EURUSD'][timestamps[0]])
                #print self.stock_market_data.stockdata['EURUSD'][timestamps[0]]
                try:

                    if(self.stock_market_data.mt5):

                        singleSample = []
                        for s in timestamps:
                            singleSample.append( self.stock_market_data.stockdata[symbol][s][MT5valueColumn] )


                            regressionResult = \
            self.stock_market_data.stockdata[symbol][self.stock_market_data.timestamps[tsIdx]][MT5valueColumn]

                    else:

                        singleSample = \
                                    (self.stock_market_data.stockdata[symbol][timestamps]).values


                        regressionResult = \
                    self.stock_market_data.stockdata[symbol][self.stock_market_data.timestamps[tsIdx]]

                    #print singleSample
                    #print regressionResult
                    #raw_input('Hit Enter to continue...')
                    if(len(singleSample) == 0 or regressionResult==None): #empty values for whatever reason
                        continue


                    #print singleSample, '\t', regressionResult
                    x.append(singleSample)
                    y.append(regressionResult)

                except IndexError:
                    #The exception happens at the first daysBack, as well as the
                    #last few timestamps
                    pass#print 'Error\t', tsIdx


        return np.array(x), np.array(y)

    def prepareData_lastDailyReturns(self, daysBack=2 ):

        #We will return these two vectors
        x, y = [],  []

        for tsIdx, _ in enumerate(self.stock_market_data.timestamps):

            if(tsIdx-daysBack<0):
                continue

            timestamps = self.stock_market_data.timestamps[tsIdx-daysBack:tsIdx+1]
            #print timestamps
            for symbol in self.stock_market_data.symbols:

                singleSample = []
                regressionResult = None

                try:
                    singleSample = self.stock_market_data.daily_returns[symbol][timestamps].values
                    regressionResult = np.sign( -1.0 + \
                            self.stock_market_data.daily_returns[symbol][self.stock_market_data.timestamps[tsIdx]])

                    if(regressionResult == 0):
                        continue

                    if(len(singleSample) == 0 or regressionResult==None): #empty values for whatever reasong
                        continue

                    #print singleSample, '\t', regressionResult
                    x.append(singleSample)
                    y.append(regressionResult)

                except IndexError:
                    #The exception happens at the first daysBack, as well as the
                    #last few timestamps
                    pass#print 'Error\t', tsIdx


        return x, y

    def prepareData_lastVolumes(self, daysBack=2):
        pass


if(__name__ == '__main__'):

    dp = DataPreparation()

    a = [345.1,347.2, 349.4, 332.2, 324.0, 329.5]
    b = dp.derivativeOf(a)
    print 'Original Series: ', a
    print 'First Derivative: ', b
    print 'Second Derivative: ', dp.derivativeOf(b) #Second derivative

    print '\n\n'

    a = [[345.1,347.2, 349.4], [332.2, 324.0, 329.5], [344.1, 354.5, 365.2]]
    b = dp.derivativeOf(a)
    print 'Original Matrix: ', a
    print 'First Derivative: ', b
    print 'Second Derivative: ', dp.derivativeOf(b) #Second derivative
