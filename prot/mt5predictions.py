'''
The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris - www.pierris.gr

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''

import sys
#import datetime as dt

import pickle

#from prot import marketsimulator as MS
from prot import marketdata_accessor as MDA
from prot import marketpredictor as md
from prot import datapreparation as dp

import time

def main():
    #startingCash = 50000
    predictionCounter = 0
    predictionBasePath = 'C:\\ProgramData\\MetaQuotes\\Terminal\\Common\\Files\\data\\'
    predictionResultPath = "C:\\Users\\gpierris\\AppData\\Roaming\\MetaQuotes\\Tester\\D0E8209F77C8CF37AD8BF550E51FF075\\Agent-127.0.0.1-3000\\MQL5\\Files\\"
    predictionFileName = 'predictionRequest_'
    predictionFileEnding = '.txt'
    svmModelFilename = 'svm_model.dat'


    options = pickle.load(open("options.p", "rb"))
    barsBack = options['barsBack']
    myPCA = options['pca']
    firstDimensions = options['dims']
    symbolStock = options['stockSymbol']
    performPCA = options['performPCA']



    #------------------------------------------------------------------------------------------------
    # Do exactly the same thing for Prediction.... FIXME: Refactor code here!
    marketData = MDA.MarketDataAccessor()

    #startDay = dt.datetime(2012, 1, 1, 16)
    #endDay = dt.datetime(2012, 9, 5, 16)
    #symbols = marketData.set_symbols([], 'sp5002012') # Just to quickly set and get the symbols
    #symbols = marketData.set_symbols([symbolStock])


    while(True):

        try:
            DATA_FILE = predictionBasePath + predictionFileName + str(predictionCounter) + predictionFileEnding
            with open(DATA_FILE):
                print 'Prediction Request #', predictionCounter+1, ' exists!'

        except IOError:

            print 'Waiting for prediction #', predictionCounter, '...'

            try:
                time.sleep(1) #seconds

            except KeyboardInterrupt:
                print 'Predictions will exit now...'
                sys.exit()

            continue #Continue in the exception of no predictionRequest available


        #Being here means that currently there is a prediction request pending ...

        marketData.load_data_from_mt5( DATA_FILE, symbolStock)
        #print '\nFetching data completed. Execution continues...\n\n'


        #Data preparation begins...
        print '\n\n\nPreparing data for SVM...'
        dataPrep = dp.DataPreparation(marketData)
        x, _ = dataPrep.prepareData_AllFeatures(daysBack=barsBack, predictionMode = True)
        print x
        #print '-------------------------------------------------'
        #print x[:3]
        #print np.size(x[:,0]), np.size(x[0,:])
        #raw_input('press enter to continue...')

        #print x[:3]
        #print np.size(x[:,0]), np.size(x[0,:])

        #x = dataPrep.derivativeOf(x)
        #x = dataPrep.appendColumns(x, dataPrep.derivativeOf(x))
        #print x

        #print x[:3]
        #print np.size(x[:,0]), np.size(x[0,:])

        #print '------------------------------'
        x = dataPrep.increaseDimensionality(x)
        #x = dataPrep.increaseDimensionality(x)
        #print x
        #print np.size(x[:,0]), np.size(x[0,:])
        #print '---------------------------------'
        #print x

        ###########################
        if(performPCA):
            x = myPCA.transform(x, firstDimensions)
        ###########################
        #raw_input('Hit Enter to continue...')

        #-----------------------------------------------------------------------------------------------

        #DIFFERENT CODE FOR PREDICTION

        #Then load svm and run prediction to create the orders.csv file

        marketPredictor = md.MarketPredictor(svmModelFilename, modelType='svm')
        marketPredictor.setMarketData(marketData)

        predictionSVM = marketPredictor.instantPredictionSVM(x)
        #raw_input('Wait....')
        print predictionSVM
        marketPredictor.savePredictionsForMT5(predictionSVM, predictionCounter, basePath = predictionResultPath)
        predictionCounter += 1

        #print 'End of Loop'

if __name__=='__main__':
    main()
